﻿using System;

namespace SDK.Common
{
    public interface ISession
    {
        Guid Id { get; }
        Guid UserId { get; }
    }
}