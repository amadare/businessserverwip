﻿using System;

namespace SDK.Common.Services.UserService
{
    public class UserData
    {
        public Guid Id;
        public string Name;
    }
}