﻿using System;
using System.Collections.Generic;

namespace SDK.Common.Services.UserService
{
    public interface IUserService : IService
    {
        event EventHandler<UserGroupsChangedInfo> OnScheduledUpdate;

        void StartRealtimeMonitoring(Guid userId);

        void StopRealtimeMonitoring(Guid userId);

        IEnumerable<UserData> StartAllUsersMonitoring();

        void LogIn();

        void LogOff();
    }
}