﻿using System;
using System.Collections.Generic;

namespace SDK.Common.Services.UserService
{
    public class GroupData
    {
        public Guid Id;
        public string Name;
        public List<Guid> UserIds;
    }
}