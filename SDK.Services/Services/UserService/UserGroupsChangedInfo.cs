﻿using System;
using System.Collections.Generic;

namespace SDK.Common.Services.UserService
{
    public class UserGroupData
    {
        public Guid GroupId;
        public Guid UserId;
    }

    public class UserGroupsChangedInfo
    {
        public IEnumerable<GroupData> GroupsAdded;
        public IEnumerable<Guid> GroupsRemoved;
        public IEnumerable<GroupData> GroupsChanged;

        public IEnumerable<UserData> UsersChanged;
        public IEnumerable<UserData> UsersAdded;
        public IEnumerable<Guid> UsersRemoved;

        public IEnumerable<UserGroupData> UsersAddedToGroups;
        public IEnumerable<UserGroupData> UsersRemovedFromGroups;
    }
}