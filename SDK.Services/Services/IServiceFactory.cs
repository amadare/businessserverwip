﻿using SDK.Common.Services.UserService;

namespace SDK.Common.Services
{
    public interface IServiceFactory
    {
        IUserService CreateUserService(ISession session);
    }
}