﻿using SDK.BLL;
using SDK.Common.Services;
using SDK.Common.Services.UserService;

namespace SimpleClient
{
    class Program
    {
        private static void Main(string[] args)
        {
            var connection = ConnectionProxy.Open("http://127.0.0.1/bs");
            var serviceFactory = new ServiceFactory(connection);
            SessionProxy session = connection.CreateSession("admin", "rosebud");
            Client client = new Client(session, serviceFactory);
            client.LogIn();

            SessionProxy anotherSession = connection.CreateSession("anotherAgent", "123");
            client.ChangeSession(anotherSession);
            client.LogIn();
        }
    }

    class Client
    {
        private SessionProxy session;
        private readonly IServiceFactory serviceFactory;
        private IServiceLocator serviceLocator;
        private IUserService userService;
        private MonitoringPlugin monitoringPlugin;

        public Client(SessionProxy session, IServiceFactory serviceFactory)
        {
            this.session = session;
            this.serviceFactory = serviceFactory;
            InitServiceLocator();
            LogIn();
            ConstructPlugins();
        }

        public void InitServiceLocator()
        {
            serviceLocator = null;
            this.userService = serviceFactory.CreateUserService(this.session);
            this.serviceLocator.AddService(this.userService);
        }

        public void ChangeSession(SessionProxy session)
        {
            this.session = session;
            InitServiceLocator();
            monitoringPlugin.ChangeSession();
        }

        public void LogIn()
        {
            userService.LogIn();
            userService.StartAllUsersMonitoring();
        }

        public void LogOff()
        {
            userService.LogOff();
        }

        private void ConstructPlugins()
        {
            monitoringPlugin = new MonitoringPlugin(serviceLocator);
        }
    }
}