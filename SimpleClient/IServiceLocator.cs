﻿using SDK.Common.Services;

namespace SimpleClient
{
    public interface IServiceLocator
    {
        TService GetService<TService>() where TService : IService;

        void AddService<TService>(TService service) where TService : IService;
    }
}