﻿using SDK.Common;
using SDK.Common.Services.UserService;
using System;
using System.Collections.Generic;

namespace SimpleClient
{
    public class MonitoringPlugin
    {
        private readonly IServiceLocator serviceLocator;
        private IUserService userService;
        private IEnumerable<UserData> Users;
        private IEnumerable<GroupData> Groups;

        public MonitoringPlugin(IServiceLocator serviceLocator)
        {
            this.serviceLocator = serviceLocator;
        }

        public void ChangeSession()
        {
            SetServices();
        }

        private void SetServices()
        {
            CleanSubscribitions();
            userService = userService = serviceLocator.GetService<IUserService>();
            userService.OnScheduledUpdate += UserServiceOnOnScheduledUpdate;
        }

        private void CleanSubscribitions()
        {
            if (userService != null)
            {
                userService.OnScheduledUpdate -= UserServiceOnOnScheduledUpdate;
            }
        }

        private void UserServiceOnOnScheduledUpdate(object sender, UserGroupsChangedInfo userGroupsChangedInfo)
        {
            //todo: apply changes from userGroupsChangedInfo
            ChangeView();
        }

        private void ChangeView()
        {
            //draw Users & Groups
        }
    }
}