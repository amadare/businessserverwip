﻿using SDK.Common;
using System;

namespace SimpleClient
{
    public class SessionChangeEventArgs : EventArgs
    {
        public ISession PreviousSession { get; }
        public ISession NewSession { get; }

        public SessionChangeEventArgs(ISession previousSession, ISession newSession)
        {
            PreviousSession = previousSession;
            NewSession = newSession;
        }
    }
}