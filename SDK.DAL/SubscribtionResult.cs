﻿using System;

namespace SDK.DAL
{
    public class SubscribtionResult<T> : SubscribtionResult
    {
        public T Data { get; set; }
    }

    public class SubscribtionResult
    {
        public Guid SubscribtionId { get; set; }
    }
}