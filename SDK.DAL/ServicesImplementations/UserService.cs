﻿using SDK.Common.Services.UserService;
using System;
using System.Collections.Generic;

namespace SDK.DAL.ServicesImplementations
{
    public class UserService : IUserService
    {
        private readonly Guid sessionId;
        private readonly UserServiceConsumer consumer;
        private readonly UserServiceSender sender;

        public event EventHandler<UserGroupsChangedInfo> OnScheduledUpdate
        {
            add { consumer.OnScheduledUpdate += value; }
            remove { consumer.OnScheduledUpdate -= value; }
        }

        public UserService(Guid sessionId, UserServiceConsumer consumer, UserServiceSender sender)
        {
            this.sessionId = sessionId;
            this.consumer = consumer;
            this.sender = sender;
        }

        public void StartRealtimeMonitoring(Guid userId)
        {
            sender.StartRealtimeMonitoring(sessionId, userId);
        }

        public void StopRealtimeMonitoring(Guid userId)
        {
            sender.StopRealtimeMonitoring(sessionId, userId);
        }

        public IEnumerable<UserData> StartAllUsersMonitoring()
        {
            var result = sender.StartAllUsersMonitoring();
            return result.Data;
        }

        public void LogIn()
        {
            var result = sender.LogIn(sessionId);
            if (result < 0)
            {
                throw new Exception("Error happened during login!");
            }
        }

        public void LogOff()
        {
            //todo: implement
            throw new NotImplementedException();
        }
    }
}