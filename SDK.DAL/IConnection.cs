﻿using System;

namespace SDK.DAL
{
    public interface IConnection
    {
        Session OpenSession(string userName, string password);
    }
}