﻿using SDK.Common;
using System;

namespace SDK.DAL
{
    public class Session : ISession
    {
        public Guid Id { get; }
        public Guid UserId { get; }
    }
}