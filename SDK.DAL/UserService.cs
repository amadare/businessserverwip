﻿using SDK.Common.Services.UserService;
using System;
using System.Collections.Generic;

namespace SDK.DAL
{
    public class UserServiceSender
    {
        public UserServiceSender(IConnection connection)
        {
        }

        //todo: clarify
        public SubscribtionResult StartRealtimeMonitoring(Guid sessionId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public SubscribtionResult StopRealtimeMonitoring(Guid sessionId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public SubscribtionResult<IEnumerable<UserData>> StartAllUsersMonitoring()
        {
            throw new NotImplementedException();
        }

        //todo: maybe replace errorcode with more meaningful data
        public int LogIn(Guid sessionId)
        {
            throw new NotImplementedException();
        }
    }

    public class RealtimeUserChangedEventArgs : EventArgs
    {
        public Guid SessionId { get; private set; }
        public UserData Data { get; private set; }

        public RealtimeUserChangedEventArgs(Guid sessionId, UserData data)
        {
            SessionId = sessionId;
            Data = data;
        }
    }

    public class UserServiceConsumer
    {
        public UserServiceConsumer(IConnection connection)
        {
        }

        public event EventHandler<RealtimeUserChangedEventArgs> OnRealtimeUserChanged;

        public event EventHandler<UserGroupsChangedInfo> OnScheduledUpdate;

        public void RealtimeUserChanged(Guid sessionId, UserData userData)
        {
            OnRealtimeUserChanged?.Invoke(this, new RealtimeUserChangedEventArgs(sessionId, userData));
        }

        public void ScheduledUpdate(UserGroupsChangedInfo info)
        {
            OnScheduledUpdate?.Invoke(this, info);
        }
    }
}