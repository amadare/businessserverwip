﻿using SDK.Common;
using SDK.DAL;
using System;

namespace SDK.BLL
{
    public class SessionProxy : ISession
    {
        private readonly Session session;

        public SessionProxy(Session session)
        {
            this.session = session;
        }

        public Guid Id
        {
            get { return session.Id; }
        }

        public Guid UserId
        {
            get { return session.UserId; }
        }
    }
}