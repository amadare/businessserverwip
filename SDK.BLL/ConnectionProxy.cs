﻿using SDK.Common;
using SDK.DAL;

namespace SDK.BLL
{
    public class ConnectionProxy
    {
        internal readonly Connection Connection;

        private ConnectionProxy(Connection connection)
        {
            this.Connection = connection;
        }

        public static ConnectionProxy Open(string address)
        {
            var connection = new Connection(address);
            return new ConnectionProxy(connection);
        }

        public SessionProxy CreateSession(string userName, string password)
        {
            var sessionId = Connection.OpenSession(userName, password);
            return new SessionProxy(sessionId);
        }
    }
}