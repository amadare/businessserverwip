﻿using SDK.Common;
using SDK.Common.Services;
using SDK.Common.Services.UserService;
using SDK.DAL;
using SDK.DAL.ServicesImplementations;

namespace SDK.BLL
{
    public class ServiceFactory : IServiceFactory
    {
        private readonly UserServiceSender userServiceSender;
        private readonly UserServiceConsumer userServiceConsumer;

        public ServiceFactory(ConnectionProxy connectionProxy)
        {
            this.userServiceSender = new UserServiceSender(connectionProxy.Connection);
            this.userServiceConsumer = new UserServiceConsumer(connectionProxy.Connection);
        }

        public IUserService CreateUserService(ISession session)
        {
            return new UserService(session.Id, userServiceConsumer, userServiceSender);
        }
    }
}